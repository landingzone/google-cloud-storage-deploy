# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 0.5.0

- minor: Add ability to use the gsutil rsync command (and the rsync -d flag) instead of gsutil cp.
- patch: Update bitbucket-pipelines.yml to use edulandingzone for the dockerhub image namespace.

## 0.4.4

- patch: Internal maintenance: change pipe metadata according to new structure

## 0.4.3

- patch: Update README: clear description for base64 encoded environment variables

## 0.4.2

- patch: Internal maintenance: Add auto infrastructure for tests.
- patch: Internal maintenance: Update pipe release process.

## 0.4.1

- patch: Internal maintenance: Add gitignore secrets.

## 0.4.0

- minor: Bump google/cloud-sdk version to 286.0.0.

## 0.3.8

- patch: Update the Readme with a new Atlassian Community link.

## 0.3.7

- patch: Internal maintenance: Add hadolint linter for Dockerfile

## 0.3.6

- patch: Updated contributing guidelines

## 0.3.5

- patch: Prevent pipe from hanging on error.

## 0.3.4

- patch: Standardising README and pipes.yml.

## 0.3.3

- patch: Added support for multiline keys files

## 0.3.2

- patch: Fixed support multi threaded directory uploads.

## 0.3.1

- patch: Fix typo in markdown link reference.

## 0.3.0

- minor: Add support for the DEBUG variable.
- minor: Switch naming conventions from task to pipes.

## 0.2.2

- patch: Use quotes for all pipes examples in README.md.

## 0.2.1

- patch: Restructure README.md to match user flow.

## 0.2.0

- minor: Modified gcloud commands to always use --quiet, to allow fast erroring and use of default settings.

## 0.1.0

- minor: Initial release of Bitbucket Pipelines GCP storage deploy pipe.

