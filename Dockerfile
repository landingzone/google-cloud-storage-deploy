FROM google/cloud-sdk:286.0.0-alpine

COPY pipe /usr/bin/

ENTRYPOINT ["/usr/bin/pipe.sh"]
